<?php
use yii\helpers\Html;

$siteLink = Yii::$app->params['main']['frontendUrl'];
?>

<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Hello,
</div>

<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: bold; margin: 0 0 10px; padding: 0;">
   Your account on Tracker has been created.
</div>

<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    <hr style="border: 0;border-bottom: 1px solid #f0f0f0;margin-top:15px;margin-bottom:15px;">
    To explore the website, please click the link below:
    <br /><br />
    <?= Html::a(Html::encode($siteLink), $siteLink); ?>
</div>
<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    If you cannot click the link, please try pasting the text into your browser.
</div>