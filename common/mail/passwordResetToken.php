<?php
use yii\helpers\Html;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['account/default/reset-password', 'token' => $user->password_reset_token]);
?>

<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Hello <?= $user->firstname ?>,
</div>

<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    <hr style="border: 0;border-bottom: 1px solid #f0f0f0;margin-top:15px;margin-bottom:15px;">
    Follow the link below to reset your password:
    <br /><br />
    <?= Html::a(Html::encode($resetLink), $resetLink); ?>
</div>
<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    If you cannot click the link, please try pasting the text into your browser.
</div>