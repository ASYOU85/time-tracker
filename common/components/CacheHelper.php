<?php
namespace common\components;

use yii\base\Component;
use Yii;

class CacheHelper {
    public static function getFromModel($model, $arrayKey = 'id', $orderBy = null, $translate = false) {
        if(!class_exists($model))
            return false;
        
        $key = self::createKey($model, $arrayKey);

        $data = Yii::$app->getCache()->get($key);
        if ($data === false) {
            $data = [];
            $allData = $model::find();
            if(!is_null($orderBy)) {
                $allData->orderBy($orderBy);
            }
            $allData = $allData->all();

            foreach($allData as $row) {
                $isOk = true;
                if($translate) {
                    $related = $row->getRelatedRecords();

                    if (isset($related['translation']) && $related['translation']) {
                        $isOk = true;
                    } else {
                        $isOk = false;
                    }
                }

                if($isOk) {
                    if(is_array($arrayKey)) {
                        $data[$row[$arrayKey[0]]][$row[$arrayKey[1]]] = $row;
                    } else {
                        $data[$row[$arrayKey]] = $row;
                    }
                }
            }

            Yii::$app->getCache()->set($key, $data);
        }

        return $data;
    }
    
    public static function clear($model, $arrayKey) {
        if(!class_exists($model))
            return false;
        
        $key = self::createKey($model, $arrayKey);
        return Yii::$app->getCache()->delete($key);
    }

    public static function clearAll() {
        Yii::$app->getCache()->flush();
    }
    
    private static function createKey($model, $arrayKey) {
        $strModel = str_replace(['\\', '/'], '_', $model);
        if($strModel[0] == '\\') {
            $strModel = substr($strModel, 1);
        }

        $strArrayKey = $arrayKey;
        if(is_array($arrayKey)) {
            $strArrayKey = implode("_", $arrayKey);
        }

        $key = 'model_' . $strArrayKey . '_' . $strModel;
        return $key;
    }
}