<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class UserRoles extends ActiveRecord
{
    const TYPE_USER = 0;
    const TYPE_ADMIN = 1;

    public static $adminRoles = null;

    public static function getAdminRoles() {
        if(!is_null(self::$adminRoles)) {
            return self::$adminRoles;
        }
        $roles = UserRoles::find()->andWhere(['type' => self::TYPE_ADMIN])->asArray()->all();
        $roles = ArrayHelper::map($roles, function($element) {
            return $element['id'];
        }, function($element) {
            return $element['id'];
        });
        self::$adminRoles = $roles;
        return $roles;
    }

    public function rules()
    {
        return [
            ['name', 'string'],
            ['name', 'required', 'on' => 'insert'],
        ];
    }

    public function scenarios()
    {
        return array(
            'update' => ['name', 'type'],
            'insert' => ['name', 'type'],
            'delete' => ['id']
        );
    }

    public function attributeLabels()
    {
        return array(
            'name' => 'Name',
            'type' => 'Access type'
        );
    }
}
