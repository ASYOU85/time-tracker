<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'language' => 'en-US',
    'sourceLanguage' => 'en-US',
    'name' => 'Tracker',
    'modules' => [
        'site' => 'frontend\modules\site\Site',
        'account' => 'frontend\modules\account\Account',
        'company' => 'frontend\modules\company\Company'
    ],
    'defaultRoute' => 'site/default/index',
    'layoutPath' => '@app/modules/site/views/layouts',
    'viewPath' => '@app/modules/site/views',
    'layout' => 'layout',
    'bootstrap' => ['settings'],
    'components' => [
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '1060815660625317',
                    'clientSecret' => 'd467ae99a3dbfa7895dc16e06646c6d9'
                ]
            ]
        ],
        'request' => [
            'enableCsrfValidation' => false,
            'cookieValidationKey' => 'CKOW35QMQHZ'
        ],
        'authManager' => [
            'class' => 'frontend\modules\rbac\components\PhpManager',
            'defaultRoles' => ['guest']
        ],
        'i18n' => [
            'translations' => [
                'modules*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'modules/site' => 'site.php',
                        'modules/account' => 'account.php',
                        'modules/company' => 'company.php'
                    ],
                ],
            ],
        ],
        'urlManager'=> [
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'ruleConfig' => ['class' => 'frontend\modules\site\components\DefaultUrlRule'],
            'rules' =>  [
                '/'=>'site/default/index',

                // Error
                'site/error/<code:\d+>/' => 'site/default/error',

                // Custom pages
                'contact/' => 'site/default/contact',

                // Main
                [
                    'class' => 'frontend\modules\site\components\MainUrlRule',
                    'pattern' => '',
                    'route' => ''
                ],

                // Default
                '<module:\w+>/<action:[0-9a-zA-Z_\-]+>'                       => '<module>/default/<action>',
                '<module:\w+>'                                                => '<module>/default',
                '<module:\w+>/<controller:\w+>/<action:[0-9a-zA-Z_\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:[0-9a-zA-Z_\-]+>'          => '<module>/<controller>/<action>'
            ]
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => '/site/default/error',
        ],
    ],
    'params' => $params,
];
