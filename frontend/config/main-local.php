<?php
if(YII_DEBUG) {
    $config = [
        'bootstrap' => ['debug'],
        'modules' => [
            'debug' => 'yii\debug\Module',
        ]
    ];
} else {
    $config = [

    ];
}

return $config;