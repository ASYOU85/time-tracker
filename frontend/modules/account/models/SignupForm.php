<?php
namespace frontend\modules\account\models;

use common\models\User;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $firstname;
    public $lastname;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('modules/account', 'This email address has already been taken.')],

            ['firstname', 'required'],
            ['lastname', 'required'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            return User::create($this->attributes);
        }

        return null;
    }

    public function attributeLabels() {
        return [
            'firstname' => Yii::t('modules/account', 'First Name'),
            'lastname' => Yii::t('modules/account', 'Last Name'),
            'password' => Yii::t('modules/account', 'Password')
        ];
    }
}
