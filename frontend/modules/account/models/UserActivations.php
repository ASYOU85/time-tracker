<?php
namespace frontend\modules\account\models;

use Yii;
use yii\db\ActiveRecord;

class UserActivations extends ActiveRecord
{
    public function rules()
    {
        return [
            [['user_id', 'token'], 'unique'],
        ];
    }

    public function scenarios()
    {
        return [
            'insert' => ['user_id', 'token']
        ];
    }
}
