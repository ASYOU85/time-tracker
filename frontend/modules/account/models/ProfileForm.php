<?php
namespace frontend\modules\account\models;

use common\models\User;
use yii\base\Model;
use Yii;
use yii\helpers\HtmlPurifier;

class ProfileForm extends Model
{
    public $id;
    public $firstname;
    public $lastname;
    public $new_email;
    public $new_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['firstname', 'required'],
            ['lastname', 'required'],
            ['new_email', 'email'],
            ['new_email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('modules/account', 'This email address has already been taken.')],
            ['new_email', 'filter', 'filter' => 'trim'],
        ];
    }

    public function save()
    {
        if ($this->validate()) {
            $user = User::findOne($this->id);
            $user->firstname = $this->firstname;
            $user->lastname = $this->lastname;
            if(!empty($this->new_password)) {
                $user->setPassword($this->new_password);
            }
            if(!empty($this->new_email)) {
                $user->email = $this->new_email;
            }

            return $user->save(false);
        }

        return null;
    }

    public function attributeLabels() {
        return [
            'firstname' => Yii::t('modules/account', 'First Name'),
            'lastname' => Yii::t('modules/account', 'Last Name'),
            'new_password' => Yii::t('modules/account', 'New Password'),
            'new_email' => Yii::t('modules/account', 'New Email')
        ];
    }
}
