<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Password Recovery';
?>

<section>
    <div class="container login">
        <div class="row ">
            <div class="center col-lg-6 col-md-6 col-sm-12 well">
                <legend>Password recovery</legend>
                <p class="text-center">You will receive token on your e-mail.</p>
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'form-signin']]); ?>
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->hint('')->label(''); ?>
                <p>&nbsp;</p>
                <?= Html::submitButton('Recover', array('class' => 'btn btn-primary btn-block')); ?>
                <?= Html::a('Sign Up', ['/account/default/signup'], ['class' => 'btn btn-info  btn-block']) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>