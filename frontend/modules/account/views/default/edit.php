<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\Url;

$this->title = "Account Settings";
?>

<div class="row">
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['id' => 'form-signin', 'options' => ['role' => 'form']]); ?>
                <?= $form->field($model, 'firstname')->textInput(['class' => 'form-control input-lg']) ?>
                <?= $form->field($model, 'lastname')->textInput(['class' => 'form-control input-lg']) ?>
                <?= $form->field($model, 'new_email')->textInput(['class' => 'form-control input-lg']) ?>
                <?= $form->field($model, 'new_password')->passwordInput(['class' => 'form-control input-lg']) ?>
                <hr />
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-info btn-lg']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
