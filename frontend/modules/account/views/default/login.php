<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Tracker - Login';
?>

<section>
    <div class="container login">
        <div class="row ">
            <div class="center col-lg-6 col-md-6 col-sm-12 well">
                <legend>Login to the dashboard</legend>
                <?php $form = ActiveForm::begin(array('id' => 'login-form', 'options' => array('class' => 'form-signin'))); ?>
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->hint('')->label(''); ?>
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->hint('')->label('');; ?>
                <?= Html::a('Forgot password?', ['/account/default/forgot']) ?>
                <p>&nbsp;</p>
                <?= Html::submitButton('Log in', array('class' => 'btn btn-success btn-block')); ?>
                <?= Html::a('<i class="fa fa-facebook"></i> Sign in with Facebook', ['/account/auth', 'authclient' => 'facebook'], ['class' => 'btn btn-primary  btn-block']) ?>
                <?= Html::a('Sign Up', ['/account/default/signup'], ['class' => 'btn btn-info  btn-block']) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>