<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'New Password';
?>

<section>
    <div class="container login">
        <div class="row ">
            <div class="center col-lg-6 col-md-6 col-sm-12 well">
                <legend>New password</legend>
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'form-signin']]); ?>
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->hint('')->label(''); ?>
                <p>&nbsp;</p>
                <?= Html::submitButton('Save', array('class' => 'btn btn-primary btn-block')); ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>