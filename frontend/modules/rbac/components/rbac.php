<?php 
use yii\rbac\Item;
use common\models\User;

return array(
    'guest' => array(
        'type' => Item::TYPE_ROLE,
        'description' => 'Guest',
		'bizRule' => NULL,
        'data' => NULL
    ),
    1 => array(
        'type' => Item::TYPE_ROLE,
        'description' => 'Admin',
        'children' => array(
            User::ROLE_ADMIN
        ),
        'bizRule' => NULL,
        'data' => NULL
    ),
    'editOwnProfile' => array(
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Edit own profile',
        'bizRule' => 'return Yii::$app->user->identity->_id == $params["user"]["id"];',
        'data' => NULL
    )
);