<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\User;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Url;

$this->title = "New company";
?>

<div class="row">
    <div class="col-lg-9 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'name')->textInput() ?>
                <div class="form-group">
                    <?= Html::submitButton('Create and continue', ['class' => 'btn btn-primary btn-lg']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>