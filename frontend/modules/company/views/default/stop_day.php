<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\User;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Url;
use yii\jui\DatePicker;
$this->title = "Stop day";
?>

<div class="row">
    <div class="col-lg-9 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'description')->textarea(['style' => 'height:200px;']) ?>
                <div class="form-group">
                    <?= Html::submitButton('Save & stop', ['class' => 'btn btn-primary btn-lg']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>