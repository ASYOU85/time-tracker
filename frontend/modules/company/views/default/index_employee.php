<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\User;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Url;

$this->title = "Company Dashboard";
?>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6">
                    <?php if($dayStatus == 3): ?>
                        <?= Html::a('<i class="fa fa-pause"></i> Pause', ['/company/default/do', 'pause' => 1], ['class' => 'btn btn-lg btn-warning']); ?>
                        <?= Html::a('<i class="fa fa-stop"></i> Stop day', ['/company/default/stop-day'], ['class' => 'btn btn-lg btn-danger']); ?>
                    <?php elseif($dayStatus == 2): ?>
                        <?= Html::a('<i class="fa fa-play"></i> Resume day', ['/company/default/do', 'pause' => 1], ['class' => 'btn btn-lg btn-success']); ?>
                        <?= Html::a('<i class="fa fa-stop"></i> Stop day', ['/company/default/stop-day'], ['class' => 'btn btn-lg btn-danger']); ?>
                    <?php elseif($dayStatus == 1): ?>
                        <?= Html::a('<i class="fa fa-play"></i> Start day', ['/company/default/do'], ['class' => 'btn btn-lg btn-success']); ?>
                    <?php endif; ?>
                </div>

                <div class="col-xs-6 text-right">
                    <?= Html::a('<i class="fa fa-plus"></i> Add previous day', ['/company/default/add-day'], ['class' => 'btn btn-lg btn-primary']); ?>
                </div>
            </div>
        </div>
    </div>

<?php if(count($log) > 0): ?>
    <strong>Average hours/day:</strong> <span class="badge"><?= $avgHours; ?></span><br />
    <strong>Total hours:</strong> <span class="badge"><?= $totalHours; ?></span><br /><br />

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Date</th>
            <th>Hours</th>
            <th>Manual?</th>
            <th>Description</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($log as $item): ?>
            <tr>
                <td><span class="label label-success"><?= date("d.m.Y", strtotime($item['date'])); ?></span></td>
                <td><?= $item['hours']; ?></td>
                <td><?= $item['is_manual'] ? 'Yes' : 'No'; ?></td>
                <td><?= nl2br($item['description']); ?></td>
                <td>
                    <?= Html::a('<i class="fa fa-eye"></i>',['/company/default/view', 'id' => $item['id']], ['class' => 'btn btn-primary pull-right']); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?php
    if($pagination->totalCount > $pagination->pageSize)
        echo \frontend\widgets\SeoLinkPager::widget([
            'pagination' => $pagination,
            'prevPageLabel' => '&lt;',
            'nextPageLabel' => '&gt;',
            'disabledPageCssClass' => null,
            'prevPageCssClass' => null,
            'nextPageCssClass' => null
        ]);
    ?>
<?php else: ?>
    <div class="alert alert-info"><h4>No days added</h4></div>
<?php endif; ?>