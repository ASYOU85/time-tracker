<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\User;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Url;
use frontend\modules\company\models\TrackerLog;

$this->title = "Day " . date("d.m.Y", strtotime($tracker['date']));
?>

<h1><?= $this->title; ?></h1>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Employee</div>

            <table class="table">
                <tr>
                    <th>Name</th>
                    <td><?= $tracker['user']['firstname'] . ' ' . $tracker['user']['lastname']; ?></td>
                </tr>
                <tr>
                    <th>E-mail</th>
                    <td><?= $tracker['user']['email']; ?></td>
                </tr>
            </table>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Details</div>

            <table class="table">
                <tr>
                    <th>Date</th>
                    <td><?= date("d.m.Y", strtotime($tracker['date'])); ?></td>
                </tr>
                <tr>
                    <th>Hours</th>
                    <td><?= $tracker['hours']; ?></td>
                </tr>
                <tr>
                    <th>Manual?</th>
                    <td><?= $tracker['is_manual'] ? 'Yes' : 'No'; ?></td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td><?= nl2br($tracker['description']); ?></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <?php if(count($tracker['log']) > 0): ?>
            <div class="panel panel-default">
                <div class="panel-heading">Log</div>

                <table class="table">
                    <tr>
                        <th>Time</th>
                        <th>Action</th>
                    </tr>
                    <?php foreach($tracker['log'] as $item): ?>
                        <tr>
                            <td><?= date("H:i", strtotime($item['time'])); ?></td>
                            <td>
                                <?php if($item['type'] == TrackerLog::START_TYPE): ?>
                                    <span class="label label-success">Start</span>
                                <?php elseif($item['type'] == TrackerLog::STOP_TYPE): ?>
                                    <span class="label label-danger">Stop</span>
                                <?php elseif($item['type'] == TrackerLog::PAUSE_TYPE): ?>
                                    <span class="label label-warning">Pause</span>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>


