<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\User;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Url;
use yii\jui\DatePicker;
$this->title = "New day";
?>

<div class="row">
    <div class="col-lg-9 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'date')->widget(DatePicker::className(), [
                    'options' => ['class' => 'form-control'],
                    'dateFormat' => 'MM/dd/yyyy'
                ]); ?>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'start_time_hours')->label('Start time (Hours)')->dropDownList($hours); ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'start_time_minutes')->label('Start time (Minutes)')->dropDownList($minutes); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'end_time_hours')->label('End time (Hours)')->dropDownList($hours); ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'end_time_minutes')->label('End time (Minutes)')->dropDownList($minutes); ?>
                    </div>
                </div>
                <?= $form->field($model, 'hours')->textInput() ?>
                <?= $form->field($model, 'description')->textarea(['style' => 'height:200px;']) ?>
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary btn-lg']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>