<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\User;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Url;

$this->title = 'Employees';
?>

<h1>Employees</h1>

<?= Html::a('<i class="fa fa-plus"></i> Add', ['/company/employees/add'], ['class' => 'btn btn-primary']); ?>
<p>&nbsp;</p>
<div class="panel panel-default">
    <div class="panel-body">
        <?php if(count($employees) > 0): ?>
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($employees as $employee): ?>
                    <tr>
                        <td><?= $employee['user']['firstname'].' '.$employee['user']['lastname']; ?></td>
                        <td><?= $employee['user']['email']; ?></td>
                        <td class="pull-right">
                            <?= Html::a('<i class="fa fa-eye"></i>',['/company/employees/view', 'id' => $employee['id']], ['class' => 'btn btn-info']); ?>
                            <?php if($employee['user_id'] != Yii::$app->getUser()->getId()): ?>
                                <?= Html::a('<i class="fa fa-minus"></i>',['/company/employees/delete', 'id' => $employee['id']], ['class' => 'btn btn-danger']); ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            if($pagination->totalCount > $pagination->pageSize)
                echo \frontend\widgets\SeoLinkPager::widget([
                    'pagination' => $pagination,
                    'prevPageLabel' => '&lt;',
                    'nextPageLabel' => '&gt;',
                    'disabledPageCssClass' => null,
                    'prevPageCssClass' => null,
                    'nextPageCssClass' => null
                ]);
            ?>
        <?php else: ?>
            <div class="alert alert-info"><h4>Employees not found</h4></div>
        <?php endif; ?>
    </div>
</div>