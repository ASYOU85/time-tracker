<?php
namespace frontend\modules\company\models;

use yii\base\Model;
use Yii;

class CompanyForm extends Model
{
    public $id;
    public $user_id;
    public $name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'on' => 'add'],
            ['name', 'string', 'max' => 120],
            ['name', 'unique', 'targetClass' => Companies::className(), 'message' => Yii::t('modules/company', 'This name has already been taken.'), 'on' => ['add']],
        ];
    }

    public function save()
    {
        if($this->scenario == 'add') {
            $company = Companies::create($this->attributes);
        } else {
            $company = Companies::edit($this->id, $this->getAttributes($this->safeAttributes()));
        }
        $this->id = $company->id;

        return $company;
    }

    public function scenarios() {
        return [
            'add' => [
                'name'
            ],
            'information' => [
                'id', 'user_id', 'name'
            ]
        ];
    }

    public function attributeLabels() {
        return [
            'name' => Yii::t('modules/company', "Name")
        ];
    }
}
