<?php
namespace frontend\modules\company\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;

class Companies extends ActiveRecord
{
    const STATUS_DISABLED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_REMOVED = 2;

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getEmployees()
    {
        return $this->hasMany(CompanyEmployees::className(), ['company_id' => 'id']);
    }

    public static function create($attributes) {
        $company = new static();
        $company->setAttributes($attributes);
        $company->user_id = Yii::$app->getUser()->getId();
        if ($company->save()) {
            return $company;
        } else {
            return null;
        }
    }

    public static function updateOrAdd($attributes, $setAttributes = null) {
        $model = static::findOne($attributes);
        if(!$model) {
            $model = new static();
        }
        if(!is_null($setAttributes)) {
            $model->setAttributes($setAttributes);
        } else {
            $model->setAttributes($attributes);
        }
        if ($model->save(false)) {
            return $model;
        } else {
            return null;
        }
    }

    public static function edit($id, $attributes) {
        $model = static::findOne($id);
        $model->setAttributes($attributes);
        if ($model->save()) {
            return $model;
        } else {
            return null;
        }
    }

    public function rules()
    {
        return [
            [['name'], 'string'],
            [['status', 'user_id'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE]
        ];
    }


    public function scenarios()
    {
        return [
            'default' => [
                'user_id', 'name', 'status'
            ]
        ];
    }
}
