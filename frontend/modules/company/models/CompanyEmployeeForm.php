<?php
namespace frontend\modules\company\models;

use yii\base\Model;
use Yii;
use common\models\User;

class CompanyEmployeeForm extends Model
{
    public $id;
    public $user_id;
    public $company_id;
    public $email;
    public $firstname;
    public $lastname;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'company_id', 'email', 'firstname', 'lastname'], 'required'],
            [['user_id', 'company_id'], 'integer'],
            ['email', 'email', 'on' => 'non-user'],
            [['email', 'firstname', 'lastname'], 'string']
        ];
    }

    public function save() {
        if($this->id) {
            $employee = CompanyEmployees::findOne($this->id);
        } else {
            $employee = new CompanyEmployees();
        }

        $employee->attributes = $this->attributes;

        if($this->scenario == 'non-user') {
            $user = new User();
            $user->generateAuthKey();
            $user->lastname = $this->lastname;
            $user->firstname = $this->firstname;
            $password = Yii::$app->security->generateRandomString(8);
            $user->setPassword($password);
            $user->email = $this->email;
            $user->status = User::STATUS_ACTIVE;
            $user->save(false);

            $employee->user_id = $user['id'];

            Yii::$app->mailer->compose('newUser', [
                'user' => $user,
                'password' => $password
            ])->setFrom([\Yii::$app->params['main']['siteEmail'] => Yii::$app->name])
                ->setTo($user['email'])
                ->setSubject(Yii::t('modules/company', 'Invite to Tracker'))
                ->send();
        }

        $employee->save(false);

        return $employee;
    }

    public function scenarios() {
        return [
            'default' => [
                'user_id', 'company_id', 'firstname', 'lastname', 'email'
            ],
            'non-user' => [
                'email', 'firstname', 'lastname', 'company_id'
            ]
        ];
    }

    public function attributeLabels() {
        return [
            'name' => Yii::t('modules/company', "Name")
        ];
    }
}
