<?php
namespace frontend\modules\company\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;

class CompanyEmployees extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public static function create($attributes) {
        $employee = new static();
        $employee->setAttributes($attributes);
        $employee->user_id = Yii::$app->getUser()->getId();
        if ($employee->save()) {
            return $employee;
        } else {
            return null;
        }
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    public function rules()
    {
        return [
            ['user_id', 'required'],
            ['company_id', 'required'],
            [['user_id', 'company_id'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => ['user_id', 'company_id']
        ];
    }
}
