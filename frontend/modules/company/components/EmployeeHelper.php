<?php
namespace frontend\modules\company\components;

use frontend\modules\company\models\CompanyEmployees;
use Yii;
use yii\base\Component;
use yii\web\HttpException;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class EmployeeHelper extends Component {
   	public static function belongsToCompany($companyId, $users_id, $boolResponse = false){
   		if(is_array($users_id)) {
   			$employees = [];
   			foreach ($users_id as $user_id) {
   				$employee = CompanyEmployees::find()->where(['company_id' => $companyId, 'user_id' => $user_id])->one();
   				if($employee) {
   					$employees[$user_id] = true;
   				}else{
   					$employees[$user_id] = false;
   				}
   			}

   			return $employees;
   		}else{
   			$employee = CompanyEmployees::find()->where(['company_id' => $companyId, 'user_id' => $users_id])->one();
   			if($employee) {
   				if($boolResponse)
   					return true;
   				return $employee;
   			}
   		}

   		return false;
   	}
    
    public static function getEmployeesArray($companyId, $surnamesOnly = false) {
        $employeesModels = CompanyEmployees::find()->where(['company_id' => $companyId])->all();

		$employees = [];
        if (!empty($employeesModels)) {
            foreach ($employeesModels as $employeesModel) {
                $employees[$employeesModel->id] = $surnamesOnly === true ? $employeesModel->user->lastname : implode(' ', [$employeesModel->user->firstname, $employeesModel->user->lastname]);
            }
        }

        return $employees;
    }
}
