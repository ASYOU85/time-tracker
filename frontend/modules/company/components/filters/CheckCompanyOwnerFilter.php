<?php
namespace frontend\modules\company\components\filters;

use frontend\modules\company\components\CompanyHelper;
use Yii;
use yii\base\ActionFilter;
use yii\web\HttpException;

// This filter checks if company exists and if current user is it's owner
class CheckCompanyOwnerFilter extends ActionFilter {

    public static function className()
    {
        return get_called_class();
    }

    public function beforeAction($action) {
        if(Yii::$app->controller->isCompanyId) {
            $companyId = Yii::$app->request->get('companyId');
            $company = CompanyHelper::getCompanyById($companyId);
            if (!$company)
                throw new HttpException(404, Yii::t('modules/company', "This company can't be found."));
            if ($company->user_id != Yii::$app->getUser()->getId())
                throw new HttpException(404, Yii::t('modules/company', "This company can't be found."));
        }
        return parent::beforeAction($action);
    }
}