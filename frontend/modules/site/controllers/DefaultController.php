<?php
namespace frontend\modules\site\controllers;

use backend\modules\currency\models\Currency;
use common\components\LanguageHelper;
use common\controllers\FrontendController;
use common\models\LoginForm;
use common\models\User;
use frontend\modules\blog\models\Blogs;
use frontend\modules\site\controllers\actions\ErrorAction;
use frontend\modules\site\models\FeedbackForm;
use frontend\modules\site\models\SignupForm;
use frontend\modules\tours\models\SearchForm;
use frontend\modules\tours\models\TourCities;
use frontend\modules\tours\models\TourCountries;
use frontend\modules\tours\models\TourLocations;
use frontend\modules\tours\models\TourOrders;
use frontend\modules\tours\models\Tours;
use Yii;
use frontend\modules\site\models\ContactForm;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;
use yii\web\HttpException;
use yii\web\Response;

class DefaultController extends FrontendController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'actions' => [
                            'login', 'error', 'index', 'contact'
                        ],
                        'roles' => ['?', '@']
                    ],
                    // deny all
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className()
            ]
        ];
    }

    public function actionLogin() {
        return $this->redirect(['/account/default/login']);
    }

    public function actionIndex()
    {
        if(Yii::$app->getUser()->isGuest) {
            $this->layout = 'landing';
            return $this->render('landing');
        } else {
            if($this->company) {
                return $this->run('/company/default/index');
            } else {
                return $this->run('/company/default/add');
            }
        }
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if(!Yii::$app->getUser()->isGuest) {
            $model->email = Yii::$app->getUser()->getIdentity()->email;
            $model->name = Yii::$app->getUser()->getIdentity()->firstname;
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['main']['siteEmail'])) {
                Yii::$app->session->setFlash('success', Yii::t('modules/site','Thank you for contacting us. We will respond to you as soon as possible.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('modules/site','There was an error sending email.'));
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
}
