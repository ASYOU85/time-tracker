<?php
use yii\helpers\Html;

$this->title = empty($code) ? $name : $code . ' - ' . $name;
?>

<h2>
    <?= $code ?>
    <small><?= Html::encode($this->title) ?></small>
</h2>
<p><?php
    if(!empty($message)) {
        echo nl2br(Html::encode($message)) ;
    } else {
        echo $name;
    }
    ?>
</p>