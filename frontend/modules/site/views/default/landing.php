<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = "Tracker: A simple way to track your employees";
?>
<div class="hero">
    <div class="navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a href="index.php" title="Tracker Logo">
                    <img src="/landing/images/logo.png" alt="Tracker Logo">
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <div class="collapse navbar-collapse">
                    <a href="<?= Url::toRoute(['/account/default/login']); ?>" class="btn btn-default btn-sm pull-right" style="margin-top: 10px;">Login</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row blurb">
            <div class="col-md-10 col-md-offset-1">
                <h1>Clear value proposition</h1>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <p>And some cool description</p>
            </div>
        </div>
        <div class="row preview">
            <div class="col-md-10 col-md-offset-1">
                <img src="/landing/images/hero-app.png" alt="Experitus Screenshot" class="img-responsive">
            </div>
        </div>
    </div>
</div>

<div class="container" style="margin-top:0;">
    <div class="section" id="full_screenshot" style="margin-bottom: 90px;">
        <h2>Question</h2>
        <div class="row">
            <div class="col-md-10 col-md-push-1 scrollme">
                <p class="italic centered">After all of this a question could hit your head: "Guys, you grabbed all my painful stuff and solved it! What left to me in this business to do? Have fun?".<br />And we would answer: "This is what Tracker.dev is all about ;)".</p>
                <a href="<?= Url::toRoute(['/account/default/signup']); ?>" class="btn btn-primary centered">Sign Up Now</a>
                <p class="centered margin-top no-margin-bottom"></p>
            </div>
        </div>
    </div>
</div>