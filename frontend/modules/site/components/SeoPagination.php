<?php
namespace frontend\modules\site\components;

use Yii;
use yii\data\Pagination;
use yii\web\Link;
use yii\web\Request;

class SeoPagination extends Pagination {
    public $forcePageParam = false;

    public function createUrl($page, $pageSize = null, $absolute = false)
    {
        $page = (int) $page;
        if (($params = $this->params) === null) {
            $request = Yii::$app->getRequest();
            $params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        if ($page > 0 || $page >= 0 && $this->forcePageParam) {
            $params[$this->pageParam] = $page + 1;
        } else {
            unset($params[$this->pageParam]);
            if(preg_match('#/page-([0-9]+)#', $this->route, $m)) {
                $this->route = str_replace("/page-" . $m[1], "", $this->route);
            }
        }
        $params[0] = $this->route === null ? Yii::$app->controller->getRoute() : $this->route;
        $urlManager = $this->urlManager === null ? Yii::$app->getUrlManager() : $this->urlManager;
        if ($absolute) {
            return $urlManager->createAbsoluteUrl($params);
        } else {
            return $urlManager->createUrl($params);
        }
    }
}
?>