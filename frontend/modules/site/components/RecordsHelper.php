<?php
namespace frontend\modules\site\components;

use yii\base\Component;
use yii\helpers\Inflector;
use Yii;

class RecordsHelper {
    public static function makeFriendlyUrl($text) {
        //$text = Inflector::slug( $text, '-', true );
        $text = self::transliteration($text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('/[^-\w]+/', '', $text);
        $text = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $text);
        $text = preg_replace('/[=\s—–-]+/u', '-', $text);
        $text = strtolower($text);
        return $text;
    }

    public static function getPhone($phoneNumber)
    {
        $phoneNumber = preg_replace('/[^0-9]/','',$phoneNumber);

        if(strlen($phoneNumber) > 10) {
            $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
            $areaCode = substr($phoneNumber, -10, 3);
            $nextThree = substr($phoneNumber, -7, 3);
            $lastFour = substr($phoneNumber, -4, 4);

            $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
        }
        else if(strlen($phoneNumber) == 10) {
            $areaCode = substr($phoneNumber, 0, 3);
            $nextThree = substr($phoneNumber, 3, 3);
            $lastFour = substr($phoneNumber, 6, 4);

            $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
        }
        else if(strlen($phoneNumber) == 7) {
            $nextThree = substr($phoneNumber, 0, 3);
            $lastFour = substr($phoneNumber, 3, 4);

            $phoneNumber = $nextThree.'-'.$lastFour;
        }

        return $phoneNumber;
    }

    public static function transliteration($title, $separator = '-') {
        $replace=array(
            "'"=>"",
            ' ' => $separator,
            "`"=>"",
            "ый" => "iy",
            "а"=>"a","А"=>"a",
            "б"=>"b","Б"=>"b",
            "в"=>"v","В"=>"v",
            "г"=>"g","Г"=>"g",
            "д"=>"d","Д"=>"d",
            "е"=>"e","Е"=>"e",
            "ё"=>"yo","Ё"=>"yo",
            "ж"=>"zh","Ж"=>"zh",
            "з"=>"z","З"=>"z",
            "и"=>"i","И"=>"i",
            "й"=>"y","Й"=>"y",
            "к"=>"k","К"=>"k",
            "л"=>"l","Л"=>"l",
            "м"=>"m","М"=>"m",
            "н"=>"n","Н"=>"n",
            "о"=>"o","О"=>"o",
            "п"=>"p","П"=>"p",
            "р"=>"r","Р"=>"r",
            "с"=>"s","С"=>"s",
            "т"=>"t","Т"=>"t",
            "у"=>"u","У"=>"u",
            "ф"=>"f","Ф"=>"f",
            "х"=>"h","Х"=>"h",
            "ц"=>"c","Ц"=>"c",
            "ч"=>"ch","Ч"=>"ch",
            "ш"=>"sh","Ш"=>"sh",
            "щ"=>"shch","Щ"=>"shch",
            "ъ"=>"","Ъ"=>"",
            "ы"=>"y","Ы"=>"y",
            "ь"=>"","Ь"=>"",
            "э"=>"e","Э"=>"e",
            "ю"=>"yu","Ю"=>"yu",
            "я"=>"ya","Я"=>"ya",
            "і"=>"i","І"=>"i",
            "ї"=>"yi","Ї"=>"yi",
            "є"=>"e","Є"=>"e"
        );
        return iconv("UTF-8", "UTF-8//IGNORE", strtr(trim($title),$replace));
    }

    public static function getNumEnding($number, $endingArray)
    {
        $number = $number % 100;
        if ($number>=11 && $number<=19) {
            $ending=$endingArray[2];
        }
        else {
            $i = $number % 10;
            switch ($i)
            {
                case (1): $ending = $endingArray[0]; break;
                case (2): !is_null($endingArray[3]) ? $ending = $endingArray[3] : $ending=$endingArray[1]; break;
                case (3):
                case (4): $ending = $endingArray[1]; break;
                default: $ending=$endingArray[2];
            }
        }
        return $ending;
    }

    public static function truncateHtml($html, $maxLength, $isUtf8=true) {
        $printedLength = 0;
        $position = 0;
        $tags = array();

        // For UTF-8, we need to count multibyte sequences as one character.
        $re = $isUtf8
            ? '{</?([a-z]+)[^>]*>|&#?[a-zA-Z0-9]+;|[\x80-\xFF][\x80-\xBF]*}'
            : '{</?([a-z]+)[^>]*>|&#?[a-zA-Z0-9]+;}';

        while ($printedLength < $maxLength && preg_match($re, $html, $match, PREG_OFFSET_CAPTURE, $position))
        {
            list($tag, $tagPosition) = $match[0];

            // Print text leading up to the tag.
            $str = substr($html, $position, $tagPosition - $position);
            if ($printedLength + strlen($str) > $maxLength)
            {
                print(substr($str, 0, $maxLength - $printedLength));
                $printedLength = $maxLength;
                break;
            }

            print($str);
            $printedLength += strlen($str);
            if ($printedLength >= $maxLength) break;

            if ($tag[0] == '&' || ord($tag) >= 0x80)
            {
                // Pass the entity or UTF-8 multibyte sequence through unchanged.
                print($tag);
                $printedLength++;
            }
            else
            {
                // Handle the tag.
                $tagName = $match[1][0];
                if ($tag[1] == '/')
                {
                    // This is a closing tag.

                    $openingTag = array_pop($tags);
                    assert($openingTag == $tagName); // check that tags are properly nested.

                    print($tag);
                }
                else if ($tag[strlen($tag) - 2] == '/')
                {
                    // Self-closing tag.
                    print($tag);
                }
                else
                {
                    // Opening tag.
                    print($tag);
                    $tags[] = $tagName;
                }
            }

            // Continue after the tag.
            $position = $tagPosition + strlen($tag);
        }

        // Print any remaining text.
        if ($printedLength < $maxLength && $position < strlen($html))
            print(substr($html, $position, $maxLength - $printedLength));

        // Close any open tags.
        while (!empty($tags))
            printf('</%s>', array_pop($tags));
    }

    public static function getHumanTimeDiff($strTime) {
        $smartDates = include("../messages/" . Yii::$app->language . "/smart_dates.php");

        $dtNow = time();
        $diff = $dtNow - $strTime;

        if ($diff >= 0 && $diff < 15) {
            // если разница меньше 15 секунд, считаем условно,
            // что событие случилось "только что"
            return $smartDates['just_now'];
        } else if ($diff >= 15 && $diff < 60) {
            // разница меньше минуты => ...секунд назад
            return $diff . " " . ($smartDates['sec_ago']);
        } else if ($diff >= 60 && $diff < 3600) {
            // разница меньше часа => ...минут назад
            return floor($diff/60) . " " . ($smartDates['min_ago']);
        } else if ($diff >= 3600 && $diff < 86400) {
            // разница меньше суток => ...часов назад
            return floor($diff/3600) . " " . ($smartDates['hours_ago']);
        } else if ($diff >= 86400 && $diff < 2592000) {
            // разница меньше месяца => ...дней назад
            return floor($diff/86400) . " " . ($smartDates['days_ago']);
        } else if ($diff >= 2592000) {
            // разница меньше года
            return $smartDates['more_month'];
        }

        return '';
    }

    public static function date_smart($date_input, $time = false) {
        $smartDates = include("../messages/" . Yii::$app->language . "/smart_dates.php");

        $months = $smartDates['months'];
        $date = $date_input;

        //Время
        if($time)
            $time = ' G:i';
        else $time = '';

        //Сегодня, вчера, завтра
        if(date('Y') == date('Y', $date)) {
            if(date('z') == date('z', $date)) {
                $result_date = $smartDates['today'] . date($time, $date);
            } elseif(date('z') == date('z', mktime(0,0,0,date('n', $date), date('j', $date)+1, date('Y', $date)))) {
                $result_date = $smartDates['yesterday'] . date($time, $date);
            } elseif(date('z') == date('z', mktime(0,0,0, date('n', $date), date('j', $date)-1, date('Y', $date)))) {
                $result_date = $smartDates['tomorrow'] . date($time, $date);
            }

            if(isset($result_date))
                return $result_date;
        }

        if(count($months) > 0) {
            $month = $months[date('n',$date)-1];
        } else {
            $month = 'F';
        }

        if(date('Y') != date('Y', $date))
            $year = $smartDates['year_format'];
        else $year = '';

        $result_date = date('j ' . $month . ' ' . $year . $time, $date);

        return $result_date;
    }
}