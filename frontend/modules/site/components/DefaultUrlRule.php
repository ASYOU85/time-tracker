<?php
namespace frontend\modules\site\components;

use yii\helpers\Url;
use yii\web\UrlRule;
use Yii;

class DefaultUrlRule extends UrlRule
{
    public function init()
    {
        parent::init();
    }

    public function parseRequest($manager, $request)
    {
        if ($this->mode === self::CREATION_ONLY) {
            return false;
        }

        if (!empty($this->verb) && !in_array($request->getMethod(), $this->verb, true)) {
            return false;
        }

        $pathInfo = $request->getPathInfo();
        $suffix = (string) ($this->suffix === null ? $manager->suffix : $this->suffix);
        if ($suffix !== '' && $pathInfo !== '') {
            $n = strlen($suffix);
            if (substr_compare($pathInfo, $suffix, -$n, $n) === 0) {
                $pathInfo = substr($pathInfo, 0, -$n);
                if ($pathInfo === '') {
                    // suffix alone is not allowed
                    return false;
                }
            } else {
                return false;
            }
        }

        if ($this->host !== null) {
            $pathInfo = strtolower($request->getHostInfo()) . ($pathInfo === '' ? '' : '/' . $pathInfo);
        }

        if (!preg_match($this->pattern, $pathInfo, $matches)) {
            if(preg_match($this->pattern . "i", $pathInfo, $matches)) {
                Yii::$app->getResponse()->redirect("/" . strtolower($pathInfo) . "/", 301);
            } else {
                return false;
            }
        }

        return parent::parseRequest($manager, $request);
    }
}