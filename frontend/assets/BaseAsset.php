<?php
namespace frontend\assets;

use common\components\LanguageHelper;
use Yii;
use yii\web\AssetBundle;

class BaseAsset extends AssetBundle
{
    const CSS_VERSION = 0.01;
    const JS_VERSION = 0.01;

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public function init() {
        parent::init();

        foreach($this->css as $i => $css_file) {
            $this->css[$i] = $css_file.'?' . self::CSS_VERSION;
        }

        foreach($this->js as $i => $js_file) {
            $this->js[$i] = $js_file.'?' . self::JS_VERSION;
        }
    }
}
