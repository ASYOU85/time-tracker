<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class LandingAsset extends BaseAsset
{
    public $css = [
        'landing/css/bootstrap.min.css',
        'landing/css/jquery-ui.min.css',
        'landing/css/animate.css',
        'landing/css/style.css',
    ];
    public $js = [
        'landing/js/jquery.js',
        'landing/js/bootstrap.min.js',
        'landing/js/headhesive.min.js',
        'landing/js/jquery-ui.min.js',
        'landing/js/scrollme.min.js',
        'landing/js/matchHeight.min.js',
        'landing/js/jquery.scrollTo.js',
        'landing/js/flickity.js',
        'landing/js/bootstrap-dropdown-on-hover.js',
        'landing/js/custom.js'
    ];
    public $depends = [

    ];
}
