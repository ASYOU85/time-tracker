<?php

use yii\db\Schema;
use yii\db\Migration;

class m151102_080349_initial extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('admin_auth', [
            'id'       => 'pk',
            'user_id'  => Schema::TYPE_INTEGER . ' NOT NULL',
            'token'    => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->createTable('settings', [
            'id'    =>  'pk',
            'name'  => 	Schema::TYPE_STRING . ' NOT NULL',
            'value' => 	Schema::TYPE_STRING . '(355) NOT NULL',
            'group' => 	Schema::TYPE_STRING . '(100) NOT NULL',
        ], $tableOptions);

        $this->createTable('user', [
            'id'                   => 'pk',
            'auth_key'             => Schema::TYPE_STRING . '(32) NOT NULL',
            'password_hash'        => Schema::TYPE_STRING . ' NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING . '(32) NULL',
            'email'                => Schema::TYPE_STRING . ' NOT NULL',
            'role'                 => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 2',
            'status'               => Schema::TYPE_SMALLINT. '(4) NOT NULL DEFAULT 10',
            'type'               => Schema::TYPE_SMALLINT. '(4) NOT NULL DEFAULT 1',
            'created_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
            'firstname'            => Schema::TYPE_STRING . ' NOT NULL DEFAULT ""',
            'lastname'             => Schema::TYPE_STRING . ' NOT NULL DEFAULT ""',
            'last_login'           => Schema::TYPE_INTEGER . ' NOT NULL',
            'facebook_id'          => Schema::TYPE_BIGINT . ' NOT NULL'
        ], $tableOptions);

        $this->createTable('user_activations', [
            'id'            => 'pk',
            'user_id'       => Schema::TYPE_INTEGER . ' NULL',
            'token'        	=> Schema::TYPE_STRING . '(120) NOT NULL',
        ], $tableOptions);

        $this->createTable('user_roles', [
            'id'          => 'pk',
            'name'        => Schema::TYPE_STRING . ' NOT NULL',
            'type'        => Schema::TYPE_SMALLINT . '(2) NOT NULL DEFAULT 0',
        ], $tableOptions);
    }

    public function down()
    {
        echo "m151102_080349_initial cannot be reverted.\n";

        return false;
    }

}
