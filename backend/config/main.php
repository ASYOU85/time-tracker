<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'name' => 'Tracker',
    'modules' => [
        'site' => 'backend\modules\site\Site',
        'account' => 'backend\modules\account\Account',
        'settings' => 'backend\modules\settings\Settings',
        'companies' => 'backend\modules\companies\Companies',
        'debug' => 'yii\debug\Module'
    ],
    'defaultRoute' => 'site/default/index',
    'layoutPath' => '@app/modules/site/views/layouts',
    'viewPath' => '@app/modules/site/views',
    'bootstrap' => ['settings', 'debug'],
    'components' => [
        'request' => [
            'enableCsrfValidation' => false,
            'cookieValidationKey' => 'CBOW35QMQHZ'
        ],
        'authManager' => [
            'class' => 'backend\modules\rbac\components\RbacManager',
            'defaultRoles' => ['guest']
        ],
        'urlManager'=> [
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'rules' =>  [
                '/'=>'site/default/index',
                '<module:\w+>/<action:[0-9a-zA-Z_\-]+>'                       => '<module>/default/<action>',
                '<module:\w+>'                                                => '<module>/default',
                '<module:\w+>/<controller:\w+>/<action:[0-9a-zA-Z_\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:[0-9a-zA-Z_\-]+>'          => '<module>/<controller>/<action>',
            ]
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ]
    ],
    'params' => $params,
];
