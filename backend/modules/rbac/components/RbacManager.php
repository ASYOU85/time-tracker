<?php 
namespace backend\modules\rbac\components;

use Yii;
use yii\rbac\Role;
use yii\rbac\PhpManager;

class RbacManager extends PhpManager
{
    public function init()
    {
        $this->itemFile = '@app/modules/rbac/components/rbac.php';
        $this->assignmentFile = '@app/modules/rbac/components/assignments.php';

        parent::init();

        if (!Yii::$app->user->isGuest) {
            // we suppose that user's role is stored in identity
            $this->assign(new Role(['name' => Yii::$app->user->identity->role]), Yii::$app->user->identity->id);
        }
    }

    protected function saveAssignments()
    {

    }

    /**
     * Temporary disabled,
     * since PhpManager updates rbac.php file on every assign() call
     *
     * @return null|void
     */
    public function save()
    {
        return null;
    }
}