<?php
use yii\rbac\Item;

return [
    // AND THE ROLES
    'guest' => [
        'type' => Item::TYPE_ROLE,
        'description' => '�����',
        'ruleRule' => NULL,
        'data' => NULL
    ],

    'user' => [
        'type' => Item::TYPE_ROLE,
        'description' => '������������',
        'children' => [
            'guest',
            'manageThing0', // User can edit thing0
        ],
        'ruleName' => 'return !Yii::$app->user->isGuest;',
        'data' => NULL
    ],

    1 => [
        'type' => Item::TYPE_ROLE,
        'description' => '�������������',
        //'ruleName' => 'return Yii::$app->user->identity->role==1;',
        'data' => NULL
    ],

    2 => [
        'type' => Item::TYPE_ROLE,
        'description' => '������������',
        //'ruleName' => 'return Yii::$app->user->identity->role==2;',
        'data' => NULL
    ],
    'admin' => [
        'type' => Item::TYPE_ROLE,
        'description' => '�������������',
        'children' => [
            'moderator',
        ],
        'ruleName' => 'return Yii::$app->user->identity->role==1;',
        'data' => NULL
    ]
];