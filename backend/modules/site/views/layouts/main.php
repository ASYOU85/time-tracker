<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\widgets\Breadcrumbs;
use frontend\widgets\Alert;

\backend\assets\AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="<?php echo Yii::$app->charset; ?>"/>
    <title><?php echo Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>
<?php
$this->registerCssFile("http://code.jquery.com/ui/1.10.3/themes/flick/jquery-ui.css");
?>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?= Html::a('<img style="height: 24px;" src="/img/logo.png" />', ['/site/index/'], ['class' => 'navbar-brand']); ?>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <?= Html::a('<i class="fa fa-user fa-fw"></i> ' . Yii::$app->user->identity->firstname, array('/account/manage/edit', 'id' => Yii::$app->user->id)); ?>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <?= Html::a('<i class="fa fa-sign-out fa-fw"></i> Log Out', ['/account/logout/']); ?>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <?= Html::a('<i class="fa fa-dashboard fa-fw"></i> Home', ['/site/index']); ?>
                    </li>
                    <li>
                        <?= Html::a('<i class="fa fa-cog fa-fw"></i> Companies', ['/companies/default/index']); ?>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-user fa-fw"></i> Users<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <?= Html::a('Manage', ['/account/manage/index']); ?>
                            </li>
                            <li>
                                <?= Html::a('Roles', ['/account/roles/index']); ?>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Settings<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <?= Html::a('System', ['/settings/site/index']); ?>
                            </li>
                            <li>
                                <?= Html::a('Other', ['/settings/other/index']); ?>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <br />
        <?php echo Alert::widget()?>
        <?php echo $content; ?>
        <br />
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<div class="ajax_container"></div>
<?php $this->endBody(); ?>

</body>

</html>
<?php $this->endPage(); ?>
