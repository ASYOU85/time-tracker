<?php
namespace backend\modules\site\controllers;

use common\components\CacheHelper;
use common\components\LanguageHelper;
use common\controllers\BackendController;
use common\models\UserRoles;
use frontend\modules\account\models\UserReviews;
use frontend\modules\blog\models\Blogs;
use frontend\modules\blog\models\BlogTags;
use frontend\modules\site\components\Sitemap;
use frontend\modules\tours\components\TourHelper;
use frontend\modules\tours\models\language\TourCitiesTranslate;
use frontend\modules\tours\models\language\TourCountriesTranslate;
use frontend\modules\tours\models\TourCategories;
use frontend\modules\tours\models\TourCities;
use frontend\modules\tours\models\TourCollections;
use frontend\modules\tours\models\TourCountries;
use frontend\modules\tours\models\TourOrders;
use frontend\modules\tours\models\TourPlaces;
use frontend\modules\tours\models\TourReviews;
use frontend\modules\tours\models\Tours;
use frontend\modules\tours\models\TourSubscription;
use frontend\modules\tours\models\TourTenders;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class DefaultController extends BackendController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index', 'clear-cache'
                        ],
                        'allow' => true,
                        'roles' => ArrayHelper::merge(['?'], UserRoles::getAdminRoles()),
                    ],
                    [
                        'allow' => false
                    ]
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if(Yii::$app->user->isGuest) {
            return $this->run("/account/default/login");
        } else {
            return $this->render('dashboard', [

            ]);
        }
    }

    public function actionClearCache() {
        CacheHelper::clearAll();
        Yii::$app->session->setFlash('success', "Cache has been successfully cleared.");
        return $this->goBack();
    }
}
