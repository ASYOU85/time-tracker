<?php
namespace backend\modules\companies\controllers;

use common\controllers\BackendController;
use common\models\UserRoles;
use frontend\modules\company\models\CompanyEmployees;
use Yii;
use yii\data\ActiveDataProvider;
use common\components\CacheHelper;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;
use backend\modules\companies\models\AdminEmployeeSearchForm;

class EmployeesController extends BackendController
{
    public function behaviors()
    {
        return array(
            'access' => array(
                'class' => \yii\filters\AccessControl::className(),
                'rules' => array(
                    array(
                        'allow' => true,
                        'actions' => array('index', 'edit', 'delete'),
                        'roles' => UserRoles::getAdminRoles()
                    ),
                    // deny all
                    array(
                        'allow' => false
                    )
                )
            )
        );
    }

    public function actionIndex($id = null)
    {
        $query = CompanyEmployees::find();
        if($id){
            $query->where(['company_id' => $id]);
        }

        $searchForm = new AdminEmployeeSearchForm();
        if($searchForm->load($_GET) && $searchForm->validate()) {
            if($searchForm->company != null){
                $query->andWhere('company_id IN (SELECT id FROM companies WHERE name LIKE :company_name)', [':company_name' => '%' . $searchForm->company . '%']);
            }
        }

        $query->orderBy(['id' => SORT_DESC]);
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        return $this->render('index', ['provider' => $provider, 'model' => $searchForm]);
    }

    public function actionDelete($id) {
        $employee = CompanyEmployees::findOne($id);
        if(!$employee) {
            throw new HttpException(404);
        }

        $employee->delete();

        CacheHelper::clearAll();

        Yii::$app->session->setFlash('success', 'You have successfully removed this employee.');

        return $this->redirect(Yii::$app->request->referrer);
    }


}