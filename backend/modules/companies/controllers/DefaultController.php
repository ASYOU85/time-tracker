<?php
namespace backend\modules\companies\controllers;

use backend\modules\companies\models\CompanyForm;
use common\controllers\BackendController;
use common\models\UserRoles;
use frontend\modules\company\models\Companies;
use Yii;
use yii\data\ActiveDataProvider;
use common\components\CacheHelper;
use yii\web\HttpException;
use backend\modules\companies\models\AdminCompanySearchForm;
use frontend\modules\company\components\CompanyHelper;

class DefaultController extends BackendController
{

    public function behaviors()
    {
        return array(
            'access' => array(
                'class' => \yii\filters\AccessControl::className(),
                'rules' => array(
                    array(
                        'allow' => true,
                        'actions' => array('index', 'edit', 'delete'),
                        'roles' => UserRoles::getAdminRoles()
                    ),
                    // deny all
                    array(
                        'allow' => false
                    )
                )
            )
        );
    }

    public function actionIndex()
    {
        $query = Companies::find()->orderBy(['id' => SORT_DESC]);

        $searchForm = new AdminCompanySearchForm();
        if($searchForm->load($_GET) && $searchForm->validate()) {
            if($searchForm->name != null){
                $query->andWhere("name LIKE :search_name", [':search_name' => '%' . $searchForm->name . '%']);
            }
            if($searchForm->firstname != null || $searchForm->lastname != null) {
                $isFirstname = false;
                if($searchForm->firstname != null) {
                    $isFirstname = true;
                    $params = 'firstname LIKE "%'.$searchForm->firstname.'%"';
                }
                if($searchForm->lastname != null) {
                    if($isFirstname) {
                        $params .= ' AND ';
                    }
                    $params .= 'lastname LIKE "%'.$searchForm->lastname.'%"';
                }
                $companyConditions = 'user_id IN (SELECT id FROM user WHERE '.$params.')';
                $query->andWhere($companyConditions);
            }
        }

        $statuses = include_once("../../frontend/messages/en-US/company_statuses.php");

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        return $this->render('index', ['provider' => $provider, 'statuses' => $statuses, 'model' => $searchForm]);
    }

    public function actionEdit($id) {
        $company = CompanyHelper::getCompanyById($id);
        if(!$company) {
            throw new HttpException(404);
        }

        $model = new CompanyForm();
        $model->id = $id;
        if ($model->load($_POST)) {
            if($model->validate()) {
                $model->save();

                Yii::$app->session->setFlash('success', "Company has been successfully saved.");
            }
        } else {
            $model->setAttributes($company->attributes);
        }

        $statuses = include_once("../../frontend/messages/en-US/company_statuses.php");

        return $this->render('edit', ['formModel' => $model, 'statuses' => $statuses]);
    }

    public function actionDelete($id) {
        $company = CompanyHelper::getCompanyById($id);
        if(!$company) {
            throw new HttpException(404);
        }

        $company->delete();

        CacheHelper::clearAll();
        
        Yii::$app->session->setFlash('success', 'You have successfully removed this company.');
        return $this->redirect(Yii::$app->request->referrer);
    }


}