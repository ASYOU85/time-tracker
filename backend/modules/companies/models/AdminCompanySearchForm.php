<?php
namespace backend\modules\companies\models;

use common\models\User;
use yii\base\Model;
use Yii;

class AdminCompanySearchForm extends Model
{
    public $name;
    public $firstname;
    public $lastname;

    public function rules()
    {
        return [
            [['name','firstname','lastname'], 'string']
        ];
    }

    public function formName() {
        return '';
    }

    public function attributeLabels() {
        return [

        ];
    }

    public function scenarios() {
        return [
            'default' => ['name','firstname','lastname']
        ];
    }
}
