<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\LanguageHelper;

$this->title = "Companies";
?>

<div class="row">
	<div class="col-lg-6">
        <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['/companies/index']]); ?>
        <div class="row">
            <div class="col-lg-8">
                <?php echo Html::activeTextInput($model, 'name', ['placeholder' => 'Company name', 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-4">
                <?php echo Html::submitButton('Enter', array('class' => 'btn btn-primary')); ?>
            </div>
            <div class="col-lg-8" style="margin-top:10px;">
                <?php echo Html::activeTextInput($model, 'firstname', ['placeholder' => 'Firstname', 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-8" style="margin-top:10px;">
                <?php echo Html::activeTextInput($model, 'lastname', ['placeholder' => 'Lastname', 'class' => 'form-control']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $this->title; ?></h1>
    </div>
</div><!-- /.row -->

<p>&nbsp;</p>

<div class="row">
    <div class="col-sm-12">
        <?php
        if($provider->count > 0) {
            echo \yii\grid\GridView::widget(
                [
                    'dataProvider' => $provider,
                    'showOnEmpty' => false,
                    'columns' => [
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'attribute' => 'id',
                            'format' => 'text'
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'format' => 'html',
                            'label' => 'User',
                            'value' => function ($model, $index, $widget) {
                                return Html::a($model->user->firstname.' '.$model->user->lastname, ['/account/manage/edit/', 'id' => $model->user->id]);
                            }
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'attribute' => 'name',
                            'format' => 'html'
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'attribute' => 'statuses',
                            'format' => 'html',
                            'value' => function ($model, $index, $widget) use($statuses) {
                                return $statuses[$model->status];
                            }
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'format' => 'html',
                            'label' => '',
                            'value' => function ($model, $index, $widget) {
                                return Html::a('<i class="fa fa-users"></i>', ['/companies/employees/index/', 'id' => $model->id]);
                            }
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'format' => 'html',
                            'label' => '',
                            'value' => function ($model, $index, $widget) {
                                return Html::a('<i class="glyphicon glyphicon-edit"></i>', ['/companies/edit/', 'id' => $model->id]);
                            }
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'format' => 'html',
                            'label' => '',
                            'value' => function ($model, $index, $widget) {
                                return Html::a('<i class="glyphicon glyphicon-remove-circle"></i>', ['/companies/delete/', 'id' => $model->id]);
                            }
                        ]
                    ]
                ]
            );
        } else {
            ?>
            <div class="alert alert-warning">You have not added any companies.</div>
        <?php
        }
        ?>
    </div>
</div><!-- /.row -->
