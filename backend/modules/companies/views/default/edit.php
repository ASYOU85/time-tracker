<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\LanguageHelper;
use yii\helpers\ArrayHelper;
$this->title = "Edit";
?>

<h1><?= $this->title; ?></h1>

<div class="panel panel-default">
    <div class="panel-body">
        <?php
        $form = ActiveForm::begin(['options' => ['id' => 'form-signup']]);
        echo $form->field($formModel, 'name')->textInput();
        echo $form->field($formModel, 'status')->dropDownList($statuses);
        ?>
        <div class="form-actions">
            <?php echo Html::submitButton('Save', array('class' => 'btn btn-lg btn-primary')); ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

