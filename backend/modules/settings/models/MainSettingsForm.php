<?php
/**
 * Settings Form
 */

namespace backend\modules\settings\models;

use Yii;
use yii\base\Model;

class MainSettingsForm extends Model
{
    public $siteName;
    public $backendUrl;
    public $frontendUrl;
    public $siteEmail;
    public $adminEmail;
    public $siteStatus;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return array(
            array(array_keys($this->getAttributes()), 'required')
        );
    }

    public function attributeLabels() {
        return array(
            'adminEmail' => 'Admin Email',
            'siteName' => 'Site Name',
            'backendUrl' => 'Backend URL',
            'frontendUrl' => 'Frontend URL',
            'siteEmail' => 'Site Email',
            'siteStatus' => 'Site Status'
        );
    }

}
