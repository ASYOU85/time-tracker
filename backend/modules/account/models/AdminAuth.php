<?php
namespace backend\modules\account\models;

use Yii;
use yii\db\ActiveRecord;

class AdminAuth extends ActiveRecord
{
    public function rules()
    {
        return [
            ['token', 'string'],
            ['user_id', 'integer']
        ];
    }

    public function scenarios()
    {
        return [
            'insert' => ['user_id', 'token']
        ];
    }
}
