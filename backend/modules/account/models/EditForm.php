<?php
namespace backend\modules\account\models;

use frontend\modules\account\components\UserHelper;
use common\models\User;
use yii\base\Model;
use Yii;

class EditForm extends Model
{
    public $id;
    public $email;
    public $old_email;
    public $firstname;
    public $lastname;
    //public $lang;
    public $status;
    public $role;
    public $password;
    public $type = User::TYPE_USER;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'checkEmail'],

            [['firstname', 'lastname'], 'string'],
            [['status', 'role'], 'integer'],

            ['password', 'string', 'min' => 6],

            ['type', 'integer'],
            ['type', 'default', 'value' => User::TYPE_USER],

            //['lang', 'in', 'range' => Yii::$app->params['langs']]
        ];
    }

    public function checkEmail() {
        if($this->old_email != $this->email) {
            $user = User::findOne(['email' => $this->email]);
            if($user) {
                $this->addError('email', "This email address has already been taken.");
            }
        }
    }

    public function save()
    {
        if ($this->validate()) {
            if($this->scenario == 'add') {
                $user = new User();
                $user->generateAuthKey();
            } else {
                $user = User::findOne($this->id);
            }
            $user->firstname = $this->firstname;
            $user->lastname = $this->lastname;
            if($this->old_email != $this->email) {
                $user->email = $this->email;
            }
            $user->status = $this->status;
            $user->role = $this->role;
            $user->type = $this->type;
            if(!empty($this->password)) {
                $user->setPassword($this->password);
            }

            return $user->save(false);
        }

        return null;
    }

    public function scenarios() {
        return [
            'default' => [
                'email', 'firstname', 'lastname',
                'status',
                'role', 'password'
            ],
            'add' => [
                'email', 'firstname', 'lastname',
                'status',
                'role', 'password'
            ]
        ];
    }
}
