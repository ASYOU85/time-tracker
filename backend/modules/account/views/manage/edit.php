<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use \yii\helpers\Url;

$this->title = $model->scenario == 'add' ? 'Add account' : 'Edit account';

$this->registerJs('initUser('. $model->id .');');
?>
<div class="page-header">
    <h1><?php echo Html::encode($this->title); ?></h1>
</div>

<div class="row">
    <div class="col-lg-7">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php
                $form = ActiveForm::begin(['id' => 'form-signin']);
                echo $form->field($model, 'email')->textInput();
                echo $form->field($model, 'status')->dropDownList(User::$statuses);
                echo $form->field($model, 'type')->dropDownList(User::$types);
                echo $form->field($model, 'role')->dropDownList($roles);
                ?>
                <div class="row">
                    <div class="col-md-10">
                        <?= $form->field($model, 'password')->textInput(); ?>
                    </div>
                    <div class="col-md-2">
                        <button id="generatePasswordButton" class="btn btn-default" style="margin-top: 24px;" type='button'><i class="glyphicon glyphicon-cog"></i></button>
                    </div>
                </div>
                <?php
                echo $form->field($model, 'firstname')->textInput();
                echo $form->field($model, 'lastname')->textInput();
                ?>

                <div class="form-actions">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']); ?>
                    <?= Html::a('Sign In', ['/account/manage/auth', 'id' => $model->id], ['class' => 'btn btn-lg btn-info']); ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
