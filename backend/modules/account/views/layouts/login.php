<?php
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $content string
 */
\backend\assets\AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?php echo Yii::$app->charset; ?>"/>
    <title><?php echo Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>
<?php
$this->registerCssFile("/css/loginLayout.css");
?>

<div class="container">
    <?php echo \frontend\widgets\Alert::widget()?>
    <?php echo $content; ?>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
