<?php
namespace backend\modules\account\controllers;

use backend\modules\account\models\AdminUserSearchForm;
use backend\modules\account\models\EditForm;
use common\controllers\BackendController;
use common\models\UserRoles;
use backend\modules\account\models\AdminAuth;
use Yii;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

class ManageController extends BackendController
{
    public function behaviors()
    {
        return array(
            'access' => array(
                'class' => \yii\filters\AccessControl::className(),
                'rules' => array(
                    array(
                        'allow' => true,
                        'actions' => array('index', 'edit', 'auth', 'add', 'get-password'),
                        'roles' => UserRoles::getAdminRoles()
                    ),
                    // deny all
                    array(
                        'allow' => false
                    )
                )
            )
        );
    }

    public function actionGetPassword() {
        if(!Yii::$app->request->isAjax) {
            throw new HttpException(403);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return Yii::$app->security->generateRandomString(8);
    }

    public function actionEdit($id) {
        $user = User::findOne($id);
        if(!$user) {
            throw new HttpException(404);
        }

        $roles = $this->getUserRoles();

        $model = new EditForm();
        $model->id = $id;
        $model->old_email = $user['email'];
        if ($model->load($_POST) && $model->validate()) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'User has been successfully saved.');
                return $this->refresh();
            }
        } else {
            $model->attributes = $user->attributes;
        }

        return $this->render('edit',
            [
                'model' => $model,
                'roles' => $roles,
                'id' => $id
            ]
        );
    }

    public function getUserRoles() {
        $roles = array();
        $rolesDb = UserRoles::find()->asArray()->all();
        foreach($rolesDb as $role) {
            $roles[$role['id']] = $role['name'];
        }

        return $roles;
    }

    public function actionAuth($id)
    {
        $id = intval($id);
        $token = Yii::$app->security->generateRandomString(10);
        $model = new AdminAuth();
        $model->user_id = $id;
        $model->token = $token;
        $model->save(false);
        return $this->redirect(Yii::$app->params['main']['frontendUrl'] . 'account/admin-auth/?token=' . $token);
    }

    public function actionIndex()
    {
        $query = User::find()->orderBy(['created_at' => SORT_DESC,'role' => SORT_DESC]);

        $searchForm = new AdminUserSearchForm();
        if($searchForm->load($_GET) && $searchForm->validate()) {
            if($searchForm->email != null){
                $userConditions = ['like', 'email', $searchForm->email];
                $query->andWhere($userConditions);
            }
        }

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $roles = $this->getUserRoles();

        return $this->render('index', ['provider' => $provider, 'model' => $searchForm, 'roles' => $roles, 'statuses' => User::$statuses]);
    }

    public function actionAdd() {
        $roles = $this->getUserRoles();

        $model = new EditForm();
        $model->scenario = 'add';
        if ($model->load($_POST)) {
            if($model->validate()) {
                if ($sModel = $model->save()) {
                    Yii::$app->session->setFlash('success', 'User has been successfully added.');
                    return Yii::$app->response->redirect(['/account/manage/index']);
                }
            }
        } else {
            $model->validate();
        }

        return $this->render('edit',
            [
                'model' => $model,
                'roles' => $roles
            ]
        );
    }
}