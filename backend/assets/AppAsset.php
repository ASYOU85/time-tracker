<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/sb-admin.css',
        'css/site.css',
        'font-awesome/css/font-awesome.min.css',
        'css/select2.css',
        'css/plugins/metisMenu/metisMenu.min.css',
        'css/plugins/timeline.css',
        'css/plugins/morris.css',
    ];
    public $js = [
        //'js/jquery-ui.min.js',
        'js/select2.min.js',
        'js/noty/jquery.noty.js',
        'js/noty/layouts/center.js',
        'js/noty/layouts/top.js',
        'js/noty/layouts/topCenter.js',
        'js/noty/themes/default.js',
        'js/moment.min.js',
        'js/plugins/metisMenu/metisMenu.min.js',
        'js/sb-admin-2.js',
        'js/functions.js'
    ];
    public $depends = [
        '\yii\web\YiiAsset',
        '\yii\bootstrap\BootstrapAsset',
        '\yii\bootstrap\BootstrapPluginAsset',
        '\yii\jui\JuiAsset'
    ];
}
